import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import iconLogo2 from '../Assets/iconLogo2.png';
import iconAccount from '../Assets/iconAccount.png';
import iconFile from '../Assets/iconFile.png';
import iconUpload from '../Assets/iconUpload.png';
import iconSatistic from '../Assets/iconSatistic.png';
import iconPremium from '../Assets/iconPremium.png';
import iconData from '../Assets/iconData.png';
import { AiOutlinePlusSquare } from 'react-icons/ai';
import { MdOutlineWavingHand } from 'react-icons/md';
import '../Css/UserpanelPage.css';
export default function UserpanelPage() {
  return (
    <div className='container-fluid'>

        <div className="row">
          {/* leftBar */}
          <div className="col-lg-4" id='leftBar'>
            <div className="text-center pt-3">
              <img src={iconLogo2} alt="UltimateCloudLogo" className='rounded w-100' />
            </div>
            <div className='d-flex pt-3'>
              <img src={iconAccount} alt="MyAccount" className='img-thumbnail' />&emsp;
              <span className='fw-bold' style={{ color: '#30E5C5' }}>My Account</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconFile} alt="MyFile" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>My File</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconUpload} alt="Upload" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>Upload</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconSatistic} alt="Satistic" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>Satistic</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconPremium} alt="Premium" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>Premium</span>
            </div>
            <br /><br /><br /><br />
            <hr className='text-white' />
            <div className='text-white p-3 m-5' id='uploadDiv'>
              <button className='text-white btn p-3'>Upload File &ensp;<AiOutlinePlusSquare /></button>
            </div>
          </div>

          {/* rightBar (2 components) */}
          <div className="col-lg-8 container-fluid overflow-auto" style={{ height: '100vh', background: '#8BEFCB' }}>

            {/* StickyHeader - 1 */}
            <div className="row bg-white" style={{ position: 'sticky', top: 0, zIndex: 10 }}>
              <div className="col-lg-12 p-3 fs-5 d-flex">
                  <p className='w-100'><MdOutlineWavingHand style={{ color: 'purple' }} /> Welcome back <b>taiproduaxe</b>, this is your userpanel.</p>
                  <button className='rounded-pill btn btn-success w-50' id='buttonStyleGreen'>Logout</button>
              </div>
            </div>

            {/* Body - 2 */}
            <div className="row h-100">
              <div className="col-lg-12 container-fluid">

                {/* AccountBalance */}
                <div className="row p-3 border">
                  <div className="col-lg-12 d-flex flex-wrap">
                    <div className='flex-fill bg-white border rounded-3 p-5 m-4 shadow-lg text-center'>
                      Account balance
                      <br />
                      <span className='fw-bold fs-5'>$2.387</span>
                      <br />
                      <button className='btn btn-success' id='buttonStylePurple'>Request payout</button>
                    </div>
                    <div className='flex-fill text-white border rounded-3 p-5 shadow-lg m-4 text-center' id='backgroundStylePurple'>
                      "Get Premium"
                      <br />
                      "Feature now!"
                      <br />
                      <button className='btn btn-success' id='buttonStyleGreen'>Upgrade Account</button>
                    </div>
                  </div>
                </div>

                {/* Used space */}
                <div className="row">
                  <div className='col-lg-12'>
                    <div className='bg-white p-3 mb-4 border d-flex rounded-3'>
                      <div className='p-3' id='centerAnElementInDiv'><img src={iconData} alt="iconData" /></div>
                      <div id='centerAnElementInDiv'>Used space<br />0 of 500MB</div>
                    </div>
                  </div>
                </div>

                {/* Button Account Settings */}
                <div className="row">
                  <div className='col-lg-12 d-flex'>
                    <button className='flex-fill btn btn-lg fw-light rounded-5 text-break' id='buttonStyleDarkPurple'>My Account Settings</button>&emsp;
                    <button className='flex-fill btn btn-lg fw-light bg-light rounded-5 text-break'>Configuration</button>
                  </div>
                </div>

                {/* My Account Settings */}
                <div className="row">
                  <div className="col-lg-12 pt-1 pb-1">
                    <div className='bg-white p-3 rounded-3'>
                      <p className='fw-bold'>My Account Settings</p>
                        <div id='myAccountSettingForm'>
                          <label htmlFor="">E-mail:</label>
                          <input className='form-control' type="email" name="email" id="email" aria-describedby="emailHelp"/>
                          <label htmlFor="">Current Passowrd</label>
                          <input className='form-control' type="password" name="current_password" id="current_password" />
                          <label htmlFor="">New password</label>
                          <input className='form-control' type="password" name="new_password" id="new_password" />
                          <label htmlFor="">Retype New password</label>
                          <input className='form-control' type="password" name="retype_password" id="retype_password" />
                        </div>
                        <br />
                        <button className='btn btn-lg' id='buttonStylePurple'>Save Settings</button>
                    </div>
                  </div>
                </div>

                {/* Footer */}
                <div className="row">
                  <div className="col-lg-12 p-3 d-flex text-white" style={{background: '#080E1B', position: 'relative'}}>
                  <div className='flex-fill'><a href="." className='text-decoration-none text-white'>Homepage</a></div>
                  <div className='flex-fill'><a href="." className='text-decoration-none text-white'>Terms of service</a></div>
                  <div className='flex-fill'><a href="." className='text-decoration-none text-white'>FAQ</a></div>
                  <div className='flex-fill'><a href="." className='text-decoration-none text-white'>Contact us</a></div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>

    </div>
  )
}
