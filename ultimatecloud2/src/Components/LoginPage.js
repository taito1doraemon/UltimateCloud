import React, { useContext, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Css/AuthenBox.css';
import { AuthContext } from './AuthProvider';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import app from '../firebase';
import toast, { Toaster } from 'react-hot-toast';
import { Navigate } from 'react-router-dom';


export default function LoginPage() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const handleLoginAccount = () => {
    // const { email, password } = e.target.elements;
    const auth = getAuth(app);
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log('user signed in: ', user);
        //Sử lý mysql ở đây
      })
      .catch((err) =>{
        console.log('error:: ', err.code + err.message);
        toast.error(err.code);
      });
  };
  const { currentUser } = useContext(AuthContext);
  if(currentUser){
    return <Navigate to="/userpanel" />
  }

  return (
    <div style={{ background: "#A7EEEE", height: "100vh" }}>
        <a href='.' className='text-success p-1 fw-bold'>Homepage</a>
        <Toaster />
        <div id='authenBox'>
          <h1 className='text-uppercase' style={{fontWeight: '900'}}>login account</h1>
          <div onSubmit={handleLoginAccount}>
            <input type="email" className="form-control p-3 m-1 rounded-pill" id="email" placeholder="Enter your email address" value={email} onChange={e => setEmail(e.target.value)} />
            <input type="text" className="form-control p-3 m-1 rounded-pill" id="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} />
            <button className='btn btn-success btn-lg w-50 rounded-pill text-break fw-bold' onClick={handleLoginAccount}>Login</button>
          </div>
          <i>Don't have an account yet? <a href="/register">Register here</a></i>
          <br />
          or
          <br />
          <a href="/forgot" className='text-danger'>Forgot password</a>
          <br />
        </div>
    </div>
  )
}
