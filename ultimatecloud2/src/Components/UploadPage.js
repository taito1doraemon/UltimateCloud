import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import iconLogo2 from '../Assets/iconLogo2.png';
import iconAccount from '../Assets/iconAccount.png';
import iconFile from '../Assets/iconFile.png';
import iconUpload from '../Assets/iconUpload.png';
import iconSatistic from '../Assets/iconSatistic.png';
import iconPremium from '../Assets/iconPremium.png';
import { AiOutlinePlusSquare, AiOutlineFileAdd } from 'react-icons/ai';
import { GrStorage } from 'react-icons/gr';
import $ from "jquery";
// import '../Css/UserpanelPage.css';
import '../Css/UploadPage.css';
export default function UploadPage() {
  var windowHeight = 0;
  var headerHeight = 0;
  var footerHeight = 0;
  var bodyHeight = 0;

  setTimeout(() => {
    windowHeight = $(window).height();
    headerHeight = $("#stickyRowHeader").height();
    footerHeight = $("#stickyRowFooter").height();
    bodyHeight = windowHeight - (headerHeight + footerHeight);
    $("#bodyRow").height(bodyHeight);
  }, 0);

  return (
    <div className='container-fluid'>

        <div className="row">
          {/* leftBar */}
          <div className="col-lg-4" id='leftBar'>
            <div className="text-center pt-3">
              <img src={iconLogo2} alt="UltimateCloudLogo" className='rounded w-100' />
            </div>
            <div className='d-flex pt-3'>
              <img src={iconAccount} alt="MyAccount" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>My Account</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconFile} alt="MyFile" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>My File</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconUpload} alt="Upload" className='img-thumbnail' />&emsp;
              <span className='fw-bold' style={{ color: '#30E5C5' }}>Upload</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconSatistic} alt="Satistic" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>Satistic</span>
            </div>
            <div className='d-flex pt-3'>
              <img src={iconPremium} alt="Premium" className='img-thumbnail' />&emsp;
              <span style={{ color: '#ECE7E7' }}>Premium</span>
            </div>
            <br /><br /><br /><br />
            <hr className='text-white' />
            <div className='text-white p-3 m-5' id='uploadDiv'>
              <button className='text-white btn p-3'>Upload File &ensp;<AiOutlinePlusSquare /></button>
            </div>
          </div>

          {/* rightBar */}
          <div className="col-lg-8 container-fluid" id='rightBar'>

            {/* Header - 1row */}
            <div className="row bg-white" id='stickyRowHeader'>
              <div className="col-lg-12 p-3 fs-5 d-flex">
                  <p className='w-100'><GrStorage style={{ color: 'purple' }} /> Balance: <b>$2.3885</b>, Used space: <b>0 of 500MB</b></p>
                  <button className='rounded-pill btn btn-success w-50' id='buttonStyleGreen'>Logout</button>
              </div>
            </div>

            {/* Body - 1row */}
            <div className="row" id='bodyRow'>
              <div className="col-lg-12" id='uploadBox'>
                <div id='uploadDrag' className='bg-white p-5 text-center'>
                    <AiOutlineFileAdd className='display-1 text-success' />
                    <br />
                    <span>Drag & drop files</span>
                    <br />
                    <span>All video formats allowed. Max upload per file is 50GB</span>
                    <br /><br />
                    <button className='btn btn-lg w-100' id='buttonStylePurple'>Browser</button>
                    {/* <input className='form-control' type="file" name="fileDrag" id="fileDrag" /> */}
                </div>
              </div>
            </div>

            {/* Footer - 1row */}
            <div className="row" id="stickyRowFooter">
                <div className="col-lg-12 border p-3 d-flex flex-row flex-warp text-white" style={{background: '#080E1B', position: 'relative'}}>
                    <div className='w-100'><a href="." className='text-decoration-none text-white'>Homepage</a></div>
                    <div className='w-100'><a href="." className='text-decoration-none text-white'>Terms of service</a></div>
                    <div className='w-100'><a href="." className='text-decoration-none text-white'>FAQ</a></div>
                    <div className='w-100'><a href="." className='text-decoration-none text-white'>Contact us</a></div>
                </div>
            </div>

          </div>
        </div>

    </div>
  )
}
