import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Css/AuthenBox.css';
import toast, { Toaster } from 'react-hot-toast';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import app from '../firebase';
import { useNavigate } from 'react-router-dom';

export default function RegisterPage() {

  const [currentUser, setCurrentUser] = useState(null);
  const navigate = useNavigate();

  const handleRegisterAccount = (e) => {
    e.preventDefault();
    const {email, password} = e.target.elements;
    const auth = getAuth(app);
    createUserWithEmailAndPassword(auth, email.value, password.value)
      .then( (userCredential) => {
        const user = userCredential.user;
        console.log('User signed in: ', user);
        setCurrentUser(true);
        toast.success('Register your account successfully.');
        setTimeout(() => {
          navigate("/userpanel");
        }, 2000);
      })
      .catch( (error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        if(errorCode === 'auth/email-already-in-use'){
          toast.error('Account already exists!');
        } else{
          console.log('error: ', errorCode + errorMessage);
          toast.error('Server error, please see console log!');
        }
      });
    // setTimeout(() => {
    //   // navigate("/");
    //   if(currentUser){
    //     console.log('CurrentUser is true');
    //     return <Navigate to="/" />
    //   }
    // }, 2000);
  };
  return (
    <div style={{ background: "#A7EEEE", height: "100vh" }}>
        <Toaster />
        <a href='.' className='text-primary p-1 fw-bold'>Homepage</a>
        <div id='authenBox'>
          <h1 className='text-uppercase' style={{fontWeight: '900'}}>register account</h1>
          <form onSubmit={handleRegisterAccount}>
            <input type="email" className="form-control p-3 m-1 rounded-pill" id="email" placeholder="Enter your email address" />
            <input type="text" className="form-control p-3 m-1 rounded-pill" id="password" placeholder="Enter your password" />
            <button className='btn btn-primary btn-lg w-50 rounded-pill text-break fw-bold' type='submit' disabled={currentUser}>Register</button>
          </form>
          <i>Already have an account? <a href="./login">Login here</a></i>
          <br />
        </div>
    </div>
  )
}
