import React, { useEffect, useState } from 'react';
import app from '../firebase';
import { getAuth, onAuthStateChanged } from 'firebase/auth';

//Lớp này được gọi khi truy cập Userpanel.js và export ra 2 biến
const auth = getAuth(app);  //Gọi Auth Service

export const AuthContext = React.createContext();
export default function AuthProvider({ children }) {  //AuthProvider đc gọi từ App.js bọc nguyên phần thân

  const [loading, setLoading] = useState(true);
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
      setLoading(false);
    });
  }, []);
  
  if(loading){
    return <p>Loading...</p>
  }

  return (//Tạo ra AuthContext.Provider giữa value các component con có thể truy xuất tới = sử dụng useContext()
    <AuthContext.Provider value={{ currentUser }}>  
      {children}
    </AuthContext.Provider>
  )
}
