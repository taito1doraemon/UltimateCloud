import { useState, useEffect } from "react";
export const useFetch = (url) => {
    const [data, setData] = useState([])
    useEffect(() => {
        const Fetdata = async () => {
            try {
                const response = await fetch();
                const data = await response.json();
                setData(data)
            } catch (error) {
                console.log(error)
            }
        }
        Fetdata()
    }, url)
    return data
}
export default useFetch