import Navbar from "../Navbar/Navbar"
import '../HomePage/homepage.css'
import useFetch from "../useFetch/useFetch"
//Lấy dữ liệu của file_name
//Chưa có dữ liệu nên chưa test được nó
const CallFile = ({ file: { file_name } }) => {
    return (
        <div className="callfile">
            <p>{file_name.toUpperCase()}</p>
        </div>
    )
}
//Lấy tất cả các dầu vào trong bảng file
//Chưa có dữ liệu nên chưa test
const CallAllDataFile = ({ files: { file_id, file_name, file_type, file_data, time_push } }) => {
    return (
        <div className="listfile">
            <p>{file_id}</p>
            <p>{file_name}</p>
            <p>{file_type}</p>
            <p>{file_data}</p>
            <p>{time_push}</p>
        </div>
    )
}
//Đổ dữ liệu từ service vào client
//Chưa có dữ liệu nên cx chưa test j cả
//Để tạm vậy fixx sau nhá
const Homepag = () => {
    const url = 'http://localhost:8000/untimatecloud/getList'
    const data = useFetch(url)
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-sm-3" id="navb">
                    <Navbar />
                </div>
                <div className="col-sm-9" id="main">
                    <div className="top">
                        <h6>Giá cả như thế này tốn bao nhiu xèn</h6>
                        <input type="button" id="log" value="Login" />
                    </div>
                    <div className="center">
                        <h1>My File</h1>
                        <div className="progress">
                            <div className="progress-bar" id="ha" role="progressbar" aria-valuenow="70"
                                aria-valuemin="0" aria-valuemax="100">
                                <span className="sr-only">70% Complete</span>
                            </div>
                        </div>
                        <input id="upload" type="button" value="Upload file" />
                    </div>
                    <div className="row">
                        <div className="col-sm-10" id="table1">
                            <input type="text" name="" id="newfile" placeholder="Nhập folder muốn tạo" /><i id="icon1" class='fas fa-clipboard-list'></i>
                            <p>{
                                data.map((file) => (
                                    <CallFile file={file} />
                                ))
                            }</p>

                        </div>
                    </div>
                    <div className="row" >
                        <div className="col-sm-10" id="table2">
                            <input type="text" id="newfile" name="" placeholder="Nhập tên tài liệu muốn tìm kiếm" /><i id="icon1" class='fas fa-clipboard-list'></i>
                            < table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><i id="icon1" class='fas fa-clipboard-list'></i></th>
                                        <th>Filename/Description</th>
                                        <th>Size</th>
                                        <th>File Upload</th>
                                        <th><i id="icon1" class='fas fa-clipboard-list'></i></th>
                                        <th><i id="icon1" class='fas fa-clipboard-list'></i></th>
                                        <th><i id="icon1" class='fas fa-clipboard-list'></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>John</td>
                                        <td>Doe</td>
                                        <td>john@example.com</td>
                                    </tr>
                                    <tr>
                                        <td>Mary</td>
                                        <td>Moe</td>
                                        <td>mary@example.com</td>
                                    </tr>
                                    <tr>
                                        <td>July</td>
                                        <td>Dooley</td>
                                        <td>july@example.com</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}
export default Homepag