import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import logo from "../Assets/logo.jpg"
import '../Css/Upload.css'
import iconAccount from "../Assets/imgAccount.png"
import iconFile from "../Assets/imgFile.png"
import iconUpload from "../Assets/iconUpload.png"
import iconSatistic from "../Assets/iconSatistic.png"
import iconPremium from "../Assets/iconPremium.png"
import iconHand from "../Assets/iconHand.png"
import iconData from "../Assets/iconData.png"

export default function UploadPage() {
  return (
    <>
       <div className="row">
        <div className=" main-menu col-lg-2" style={{backgroundColor: '#1F1760' }}>
            <div>
                <img className="logo" src={logo} alt=""/>
            </div>
            <ul>
                <a href=".">
                    <li className="menu-item"> <img src={iconAccount} alt='' width="10%" /> My Account</li>
                </a>
                <a href=".">
                    <li className="menu-item"><img src={iconFile} alt='' width="10%" /> My Files</li>
                </a>
                <a href=".">
                    <li className="menu-item"><img src={iconUpload} alt='' width="10%" /> Upload</li>
                </a>
                <a href=".">
                    <li className="menu-item"><img src={iconSatistic} alt='' width="10%" /> Satistic</li>
                </a>
                <a href=".">
                    <li className="menu-item"><img src={iconPremium} alt='' width="10%" /> Premium</li>
                </a>

            </ul>
            <hr/>

            <input className="uploadfile-left" type="button" value="Upload file"/>
        </div>

        <div className="col-lg-10">
            <div className="row" style={{height:'60px' , borderBottom: '#dcd9eb 1px solid'}}>

                <div className="col-lg-10" style={{paddingTop:'10px' }}>
                    <span>
                        Balance:
                       <strong>$0</strong>,
                       Used space:
                       <strong>0.00 of 1024 GB</strong>,
                       Traffic available today:
                       <strong>Unlimited</strong>
                       </span>
                </div>
                <div className="col-lg-2">
                    <input className="logout" type="button" value="  Logout  "/>
                </div>
            </div>
            <div className="content_action">
                <p className="content_action-1">Drag & drop files</p>
                <p className="content_action-2">All video formats allowed. Max upload per file is 50GB</p>
                <input className="browser" type="button" value="   Browser   " />
            </div>

            <div className="footer">
                
                    <div>Homepage</div>
                    <div>Tems of service</div>
                    <div>FAQ</div>
                    <div>Contact us</div>
                
            </div>
        </div>
    </div>
    </>
  )
}
