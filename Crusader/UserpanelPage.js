import React, { useContext } from 'react';
import { AuthContext } from './AuthProvider';
import { Navigate } from "react-router-dom";
import app from '../firebase';
import { getAuth, signOut } from 'firebase/auth';
import '../Css/userPanelPage.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import logo from "../Assets/logo.jpg"
import iconAccount from "../Assets/imgAccount.png"
import iconFile from "../Assets/imgFile.png"
import iconUpload from "../Assets/iconUpload.png"
import iconSatistic from "../Assets/iconSatistic.png"
import iconPremium from "../Assets/iconPremium.png"
import iconHand from "../Assets/iconHand.png"
import iconData from "../Assets/iconData.png"

export default function UserpanelPage() {

  const auth = getAuth(app);
  const { currentUser } = useContext(AuthContext);

  if (!currentUser) {
    return <Navigate to="/login" />;
  }

  return (
    <div className='container-fluid row'>

      <div className='col-sm-5 col-md-3 col-lg-3 pnl-left'>
        <div className='img-logo'>
          <img src={logo} alt="" width="100%" />
        </div>

        <div className='pnl-bottom'>
          <ul className='nav flex-column'>
            <li className='nav-item' style={{ marginBottom: '5%' }}><h5><a href='.'><img src={iconAccount} alt='' width="10%" />My Account</a></h5></li>
            <li className='nav-item' style={{ marginBottom: '10%' }}><h5><a href='.'><img src={iconFile} alt='' width="10%" />My Files</a></h5></li>
            <li className='nav-item' style={{ marginBottom: '10%' }}><h5><a href='.'><img src={iconUpload} alt='' width="10%" />Upload</a></h5></li>
            <li className='nav-item' style={{ marginBottom: '10%' }}><h5><a href='.'><img src={iconSatistic} alt='' width="10%" />Satistic</a></h5></li>
            <li className='nav-item'><h5><a href='.'><img src={iconPremium} alt='' width="10%" />Premium</a></h5></li>
          </ul>
        </div>
      </div>

      <div className='col-sm-7 col-md-9 col-lg-9 pnl-right'>
        <header className='row'>
          <div className='col-md-10 col-lg-10'>
            <p><img src={iconHand} alt='' width="4%" /> Welcome back taiproduaxe, this is your userpanel</p>
          </div>

          <div className='col-md-2 col-lg-2'>
            <button className='btn-logout' onClick={() => signOut(auth)}>Logout</button>
          </div>
        </header>

        <section className='row'>
          <div className='col-md-6 col-lg-6 pnl-balance'>
            <h5>Account balance</h5>
            <h5><strong>$0.0</strong></h5>
            <input className='btn-recharge-more' type="button" value="Recharge more" />
          </div>

          <div className='col-md-6 col-lg-6 pnl-get-premium'>
            <h2>Get Premium Features now!</h2>
            <input className='btn-upgrade-account' type="button" value="Upgrade Account!" />
          </div>

          <div className='col-md-12 col-lg-12 pnl-used-space'>
            <div className='row'>
              <div className='col-md-6 col-lg-6 pnl-icon-data'>
                <img src={iconData} alt='' width="100%" />
              </div>

              <div className='col-md-6 col-lg-6'>
                <h4>Used space</h4>
                <h4 className='row'><strong>0 of 500 MB</strong></h4>
              </div>
            </div>
          </div>

          <div className='col-md-6 col-lg-6'>
            <input className='btn-my-account-settings' type="button" value="My Account Settings" />
          </div>

          <div className='col-md-6 col-lg-6'>
            <input className='btn-configuration' type="button" value="Configuration" />
          </div>

          <div className='col-md-12 col-lg-12 pnl-form-account'>
            <h5>My Account Settings</h5>
            <form action='/'>
              <label className='form-label'>E-mail</label>
              <input className='form-control' type="email" name='email' />
              <label className='form-label'>Current Password</label>
              <input className='form-control' type="password" name='currentPassword' />
              <label className='form-label'>New Password</label>
              <input className='form-control' type="password" name='newPassword' />
              <label className='form-label'>Retype New password</label>
              <input className='form-control' type="password" name='retypePassword' />
              <input className='btn-save' type="submit" value="Save Settings" />
            </form>
          </div>
        </section>

        <footer className='row'>
          <a href='.' className='col-md col-lg'>Homepage</a>
          <a href='.' className='col-md col-lg'>Terms of service</a>
          <a href='.' className='col-md col-lg'>FAQ</a>
          <a href='.' className='col-md col-lg'>Contact us</a>
        </footer>
      </div>
    </div>
  )
}