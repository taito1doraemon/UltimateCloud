const express = require('express');
const mysql = require("mysql");
const app = express();
var cors = require("cors");
app.use(express.json());


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


const db = mysql.createConnection({
    user: "root",
    host: "localhost",
    password: "",
    database: "loginsystem",
});

const country_name = "vi-VN2";
const population = 12300069;
//Lấy toàn bộ dữ liệu trong bảng file
app.get("/untimatecloud/getList", function (req, res) {
    db.query('select * from file', function (err, file) {
        if (!err) {
            res.json(file)
        }
        else {
            res.status(400)
                .json("Lỗi lấy dữ liệu nhé bạn")

        }
    })
})
//Thêm file vào bảng file nhá
app.post("/untilmatecloud/addFile", function (req, res) {
    var data = req.body
    db.query('insert into file set ?', data, function (err, file) {
        if (err) {
            res.status(400)
                .json("Lỗi thêm dữ liệu")
        }
        else {
            res.json(data)
        }
    })
})
//Cập nhật dữ liệu lên
app.post("/untimatecloud/updateFile", function (req, res) {
    var data = req.body
    db.query("update file set ? where file_id=?", req.params.file_id, data, function (err, file) {
        if (err) {
            res.status(400).json("Lỗi cập nhật dữ liệu")
        }
        else {
            res.json(file)
        }
    })
})
//Xóa dữ liêu file
app.delete("/untimatecloud/deleteFile/`${file_id}`", function (req, res) {
    var data = { file_id: req.body.file_id }
    db.query('delete from file where file_id=?', req.body.file_id, data, function (err, file) {
        if (err) {
            res.status(400)
                .json({ 'err': "Lỗi xóa dữ liệu nhá" })
        }
        else {
            res.json("Xóa thành công ")
        }
    })
})
//Tìm kiếm dữ liệu
app.get("untimatecloud/search", function (req, res) {
    var data = req.body.file_id
    db.query('select from file where file_id=?', data, function (err, file) {
        if (err) {
            res.status(400)
                .json({ 'err': "Lỗi tìm dữ liệu" })
        }
        else {
            res.json(file)
        }
    })
})
//Đăng nhập
app.post('/register', (req, res) => {

    const username = req.body.username; //Get path variable if request call to this post
    const password = req.body.password; //Get path variable if request call to this post

    db.query("INSERT INTO users (username,password) VALUES (?,?)",
        [username, password],
        (err, rs) => {
            if (err) {
                console.log(err);
            }
            res.send(rs);
        });
});
app.get("/hello",function (req,res) {
    res.json("Hello a dương")
})
var service=app.listen(8000,function (host,port) {
    var host=service.address().address;
    var port=service.address().port;
    console.log("Service suscess nhá",host,port)
})
